import React from 'react';
import {StyleSheet, SafeAreaView, View, TouchableOpacity} from 'react-native';
import {useState} from 'react';

export default function App() {
  const [color, setColor] = useState('#B9DF4B');
  return (
    <SafeAreaView>
      <View style={[styles.big, {backgroundColor: color}]}></View>
      <View style={styles.box}>
        <TouchableOpacity
          onPress={() => setColor('#B9DF4B')}
          style={[
            styles.small,
            {backgroundColor: '#B9DF4B'},
          ]}></TouchableOpacity>
        <TouchableOpacity
          onPress={() => setColor('#62A1FF')}
          style={[
            styles.small,
            {backgroundColor: '#62A1FF'},
          ]}></TouchableOpacity>

        <TouchableOpacity
          onPress={() => setColor('#54E5B1')}
          style={[
            styles.small,
            {backgroundColor: '#54E5B1'},
          ]}></TouchableOpacity>
        <TouchableOpacity
          onPress={() => setColor('#62A1FF')}
          style={[
            styles.small,
            {backgroundColor: '#FF6262'},
          ]}></TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  big: {
    width: 300,
    height: 300,
  },
  small: {
    width: 70,
    height: 70,
  },
  box: {
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 20,
  },
});
